import React, { useEffect, useState } from "react";
import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { connect } from "./redux/blockchain/blockchainActions";
import { fetchData } from "./redux/data/dataActions";
import * as s from "./styles/globalStyles";
import styled from "styled-components";

export const StyledButton = styled.button`
  padding: 8px;
`;

export const StyledInput = styled.input`
  padding: 8px;
`;

function App() {
  const dispatch = useDispatch();
  const blockchain = useSelector((state) => state.blockchain);
  const blockchainData = useSelector((state) => state.data);
  const [collectionName, setCollectionName] = useState('');
  const [collectionSymbol, setCollectionSymbol] = useState('');
  const [claimingNft, setClaimingNft] = useState(false);
  const [collectionAddress, setCollectionAddress] = useState(false);
  const [transactionHash, setTransactionHash] = useState(null);
  const [mintedEvent, setMintedEvent] = useState({});

  const createCollection = () => {
    setClaimingNft(true);
    blockchain.smartContract.methods
      .createCollection(collectionName, collectionSymbol)
      .send({
        from: blockchain.account,
        value: '0x0',
      })
      .once("error", (err) => {
        console.log(err);
        console.log('err', err);
        setClaimingNft(false);
      })
      .then((receipt) => {
        console.log('receipt', receipt);
        setClaimingNft(false);
        dispatch(fetchData(collectionSymbol));
      });
  };

  const mintNFT = () => {
    setClaimingNft(true);
    blockchain.smartContract.methods
      .mint(collectionAddress)
      .send({
        from: blockchain.account,
        value: '0x0',
      })
      .once("error", (err) => {
        console.log('err', err);
        setClaimingNft(false);
      })
      .then((receipt) => {
        console.log('receipt', receipt);
        setClaimingNft(false);
        setTransactionHash(receipt.transactionHash);
        setMintedEvent(receipt.events.TokenMinted.returnValues);
      });
  };

  useEffect(() => {
    console.log('blockchainData', blockchainData);
    if (blockchainData.collection !== null) {
      setCollectionAddress(blockchainData.collection);
    }
  }, [blockchain.collection, blockchainData, blockchainData.collection, setCollectionAddress]);

  return (
    <s.Screen>
      {blockchain.account === "" || blockchain.smartContract === null ? (
        <s.Container flex={1} ai={"center"} jc={"center"}>
          <s.TextTitle>Connect to the Blockchain</s.TextTitle>
          <s.SpacerSmall />
          <StyledButton
            onClick={(e) => {
              e.preventDefault();
              dispatch(connect());
            }}
          >
            CONNECT
          </StyledButton>
          <s.SpacerSmall />
          {blockchain.errorMsg !== "" ? (
            <s.TextDescription>{blockchain.errorMsg}</s.TextDescription>
          ) : null}
        </s.Container>
      ) : null}
      { blockchain.smartContract && !collectionAddress && !transactionHash ? (
        <s.Container flex={1} ai={"center"} jc={"center"}>
          <s.TextTitle style={{ textAlign: "center" }}>
            Create collection
          </s.TextTitle>
          <s.SpacerXSmall />
          <s.TextDescription>
            Collection name
          </s.TextDescription>
          <StyledInput
            value={collectionName}
            onChange={(e) => setCollectionName(e.target.value)}
          />
          <s.SpacerSmall />
          <s.TextDescription>
            Collection symbol
          </s.TextDescription>
          <StyledInput
            value={collectionSymbol}
            onChange={(e) => setCollectionSymbol(e.target.value)}
          />
          <s.SpacerSmall />
          <StyledButton
            disabled={claimingNft ? 1 : 0}
            onClick={(e) => {
              e.preventDefault();
              createCollection();
            }}
          >
            {claimingNft ? "CREATING" : "CREATE"}
          </StyledButton>
          <s.SpacerSmall />
        </s.Container>
      ) : null}

      { blockchain.smartContract && collectionAddress && !transactionHash ? (
        <s.Container flex={1} ai={"center"} jc={"center"}>
          <s.TextTitle style={{ textAlign: "center" }}>
            Mint collection
          </s.TextTitle>
          <s.SpacerXSmall />
          <s.TextDescription>
            Collection address
          </s.TextDescription>
          <StyledInput
            value={collectionAddress}
            onChange={(e) => setCollectionAddress(e.target.value)}
          />
          <s.SpacerSmall />
          <StyledButton
            disabled={claimingNft ? 1 : 0}
            onClick={(e) => {
              e.preventDefault();
              mintNFT();
            }}
          >
            {claimingNft ? "MINTING" : "MINT"}
          </StyledButton>
          <s.SpacerSmall />
        </s.Container>
      ) : null}

      { blockchain.smartContract && collectionAddress && transactionHash ? (
        <s.Container flex={1} ai={"center"} jc={"center"}>
          <s.TextTitle style={{ textAlign: "center" }}>
            Congratulations, you've minted the NFT
          </s.TextTitle>
          <s.SpacerXSmall />
          <s.TextDescription>
            Transaction ID: {transactionHash}
          </s.TextDescription>
          <s.SpacerSmall />
          <s.TextDescription>
            Collection: {mintedEvent.collection}
          </s.TextDescription>
          <s.SpacerSmall />
          <s.TextDescription>
            Token ID: {mintedEvent.tokenId}
          </s.TextDescription>
          <s.SpacerSmall />
          <s.TextDescription>
            Token Uri: {mintedEvent.tokenUri}
          </s.TextDescription>
          <s.SpacerSmall />
        </s.Container>
      ) : null}

    </s.Screen>
  );
}

export default App;
